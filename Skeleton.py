#!/usr/bin/env python
# -*- coding: utf-8 -*-

#| Skeleton.py --- a Skeleton for Python's program
#|
#| By Thierry Leurent <thierry.leurent@asgardian.be>
#|
#| (C) 2019 Thierry Leurent
#|

#| Notice :
#|   - Logs management from :
#|      - http://sametmax.com/ecrire-des-logs-en-python/
#|      - https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/
#|   - Script arguments and configuration file :
#|      - https://docs.openstack.org/oslo.config/latest/

import os
import sys

#| Arguments and Configuration file.
from oslo_config import cfg
from oslo_config import types

#| Logging.
import logging
from logging.handlers import RotatingFileHandler

#| EWS.
from exchangelib import DELEGATE, IMPERSONATION, Account, Credentials, ServiceAccount, \
    Configuration, NTLM, GSSAPI, Build, Version, Folder
from exchangelib import UTC, UTC_NOW, EWSDateTime
from exchangelib.errors import RelativeRedirect, ErrorItemNotFound, ErrorInvalidOperation, AutoDiscoverRedirect, \
    AutoDiscoverCircularRedirect, AutoDiscoverFailed, ErrorNonExistentMailbox, UnknownTimeZone, \
    ErrorNameResolutionNoResults, TransportError, RedirectError, CASError, RateLimitError, UnauthorizedError, \
    ErrorInvalidChangeKey, ErrorAccessDenied, \
    ErrorFolderNotFound, ErrorInvalidRequest, SOAPError, ErrorInvalidServerVersion, NaiveDateTimeNotAllowed, \
    AmbiguousTimeError, NonExistentTimeError, ErrorUnsupportedPathForQuery, \
    ErrorInvalidValueForProperty, ErrorPropertyUpdate, ErrorDeleteDistinguishedFolder, \
    ErrorNoPublicFolderReplicaAvailable, ErrorServerBusy, ErrorInvalidPropertySet, ErrorObjectTypeChanged, \
    ErrorInvalidIdMalformed, SessionPoolMinSizeReached

import pprint

class Application(object):
    Version = "0.0.1"

    def __init__(self, Logger=None):
        #| Create an object logger to write logs.
        self.Logger = Logger or logging.getLogger(__name__)
        #| Add a second handler to write logs into the console.
        stream_handler = logging.StreamHandler()
        stream_handler.setLevel(logging.DEBUG)
        self.Logger.addHandler(stream_handler)

        self.Configuration=self.ReadConfig(Version=self.Version)

        if (self.Configuration.LOGGING.Level != 'Notset'):
            #| My format.
            format = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
            #| Log to a file in mode 'append' and max size 1Mo
            logFile= os.path.join(self.Configuration.LOGGING.directory,
                                  self.Configuration.LOGGING.filename)

            if (self.Configuration.LOGGING.encoding is not None):
                file_handler = RotatingFileHandler(logFile,
                                                   mode=self.Configuration.LOGGING.mode,
                                                   maxBytes=self.Configuration.LOGGING.MaxBytes,
                                                   backupCount=self.Configuration.LOGGING.backupCount,
                                                   #encoding=self.Configuration.LOGGING.encoding,
                                                   delay=self.Configuration.LOGGING.delay)
            else:
                file_handler = RotatingFileHandler(logFile,
                                                   mode=self.Configuration.LOGGING.mode,
                                                   maxBytes=self.Configuration.LOGGING.MaxBytes,
                                                   backupCount=self.Configuration.LOGGING.backupCount,
                                                   delay=self.Configuration.LOGGING.delay)
            file_handler.setLevel(getattr(logging,
                                          self.Configuration.LOGGING.Level.upper()))
            file_handler.setFormatter(format)
            self.Logger.addHandler(file_handler)



    def  ReadConfig(self, Version=0, ConfigFileName=None, LogFileName=None):
        if not (LogFileName):
            LogFileName = self.getLogFileName()

        CONF = cfg.ConfigOpts()

        opt_LOGGING_group = cfg.OptGroup(name='LOGGING',
                                         title='LOGGING Options'
        )
        LOGGING_opts = [
            cfg.StrOpt('directory',
                       default='./',
                       help='Where the log files must be saved.'),
            cfg.StrOpt('filename',
                       default=LogFileName,
                       help='The name of the log file.'),
            cfg.StrOpt('mode',
                       default='a',
                       help='By default, we append the file.'),
            cfg.IntOpt('MaxBytes',
                       default=1000000,
                       help='Max size of the log file (1Mo).'),
            cfg.IntOpt('backupCount',
                       default=0,
                       help='Max number of backup files.'),
            cfg.StrOpt('encoding',
                       default='None',
                       help='Use another encoding for the file.'),
            cfg.BoolOpt('delay',
                       default=False,
                       help='Open the file right now or wait to the first call.'),
            cfg.StrOpt('Level',
                       choices=['Notset',
                                 'Debug',
                                 'Info',
                                 'Warning',
                                 'Error',
                                 'Critical'],
                       default='Notset',
                       help='Set debug level.'),
        ]


        CONF.register_group(opt_LOGGING_group)
        CONF.register_opts(LOGGING_opts, opt_LOGGING_group)
        CONF.register_cli_opts(LOGGING_opts, opt_LOGGING_group)

        #| Customize the parameters and the configuration file.
        #  -----------------------------------------------------------------------

        # To add a new group of options.
        # opt_MYGROUP_group = cfg.OptGroup(name='MYGROUP',
        #                                  title='MYGROUP Options'
        # )
        # MYGROUP_opts = [
        #     cfg.StrOpt('directory',
        #                default='./',
        #                help='Where the log files must be saved.'),
        #     ]

        if not (ConfigFileName):
            ConfigFileName = self.getConfigFileName()
        try:
            CONF(version=Version,
                      default_config_files=[ConfigFileName]
            )
        except cfg.ConfigFilesNotFoundError:
            CONF(version=Version,
                      default_config_files=[]
            )
        except Exception as e:
            self.Logger.error("OSLO-Config: %s" % e)
            sys.exit("ERROR: %s" % e)
        except RuntimeError as e:
            self.Logger.error("OSLO-Config: %s" % e)
            sys.exit("ERROR: %s" % e)
        return CONF

    def getConfigFileName(self):
        return os.path.join(os.path.dirname(os.path.abspath(__file__)), ('.').join(( os.path.splitext(os.path.basename(sys.argv[0]))[0],
                           'cfg')))
    def getLogFileName(self):
        return ('.').join(( os.path.splitext(os.path.basename(sys.argv[0]))[0],
                           'log'))

if __name__ == "__main__":

    Application()
